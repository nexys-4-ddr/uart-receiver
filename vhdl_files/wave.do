onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_uartrecubuntuterminal/clk
add wave -noupdate /tb_uartrecubuntuterminal/rst
add wave -noupdate /tb_uartrecubuntuterminal/rx
add wave -noupdate /tb_uartrecubuntuterminal/recData
add wave -noupdate /tb_uartrecubuntuterminal/newData
add wave -noupdate /tb_uartrecubuntuterminal/TbClock
add wave -noupdate /tb_uartrecubuntuterminal/TbSimEnded
add wave -noupdate -divider State
add wave -noupdate /tb_uartrecubuntuterminal/dut/line__24/state
add wave -noupdate /tb_uartrecubuntuterminal/dut/line__24/next_state
add wave -noupdate /tb_uartrecubuntuterminal/dut/line__24/received_bit
add wave -noupdate /tb_uartrecubuntuterminal/dut/line__24/cnt
add wave -noupdate /tb_uartrecubuntuterminal/dut/line__24/tmp_recData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 298
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {2025 ps}
