library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uartRecUbuntuTerminal is
	generic(
		pulse2cnt : integer := 5199 ;-- @ 50MHz 9600 br ==> (104 * 10^-6)/(0.2*10^-9) -1 
		half_pulse2cnt : integer := 2600 -- (pulse2cnt+1)/2
	);
	port(
		clk : in std_logic;
		rst : in std_logic; -- low active rst
		rx :  in std_logic;
		--- Out
		clk_debug : out std_logic; -- DEGUG
		recData : out std_logic_vector(7 downto 0);
		newData : out std_logic
	);
end uartRecUbuntuTerminal;


architecture uartArc of uartRecUbuntuTerminal is
	begin -- begin architecture
	
	process(clk, rst)
	type state_type is (at_rst, starting ,waiting, opening_com_ch ,reciving_data, error_state );
	variable state : state_type := waiting;
	variable next_state : state_type := waiting;
	variable received_bit : integer range 0 to 7;
	variable cnt : integer range 0 to pulse2cnt;
	variable tmp_recData : std_logic_vector(7 downto 0);
	-- Debug
	variable debug : std_logic := '0';
	begin -- begin process
	
	
	if(rst='0') then
		state:= at_rst;
		next_state := starting;
	elsif(rising_edge(clk)) then
	    -- Debug
	    debug := not debug;
	    clk_debug <= debug;
	
		state := next_state;
		case(state) is
		
			when starting =>
				newData <= '0';
				if(rx='0') then
					next_state := error_state;
				else
					next_state := waiting;
				end if;
				
			when waiting =>
				received_bit := 0 ;
				newData <= '0';
				if(rx='0') then
					next_state := opening_com_ch;
				end if;
			
			
			when opening_com_ch =>
			
				if (cnt /= pulse2cnt) then
					cnt := cnt +1;
				else
					cnt := 0;
					next_state:= reciving_data;
				end if;
			
			when reciving_data => 
			
				if (cnt /= pulse2cnt) then
					cnt := cnt +1;
				else
					cnt := 0;
					if(received_bit /= 7 ) then
						received_bit := received_bit +1;
					else
						received_bit := 0;
						recData <= tmp_recData;
						newData <= '1';
						next_state := waiting;
					end if;	
				end if;
				
				if(cnt = half_pulse2cnt) then
					tmp_recData(received_bit) := rx; 
				end if;
			
			
			when others =>
				if(rx='1') then
					next_state := waiting;
				end if;
			
			
		end case;
	
	end if;
	
	end process;

end architecture;
