library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_uartRecUbuntuTerminal is
end tb_uartRecUbuntuTerminal;

architecture tb of tb_uartRecUbuntuTerminal is

    component uartRecUbuntuTerminal
        port (clk     : in std_logic;
              rst     : in std_logic;
              rx      : in std_logic;
              recData : out std_logic_vector (7 downto 0);
              newData : out std_logic);
    end component;

    signal clk     : std_logic;
    signal rst     : std_logic;
    signal rx      : std_logic;
    signal recData : std_logic_vector (7 downto 0);
    signal newData : std_logic;

    constant TbPeriod : time := 20 ns;
    constant bitTime : time := 104 us;
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : uartRecUbuntuTerminal
    port map (clk     => clk,
              rst     => rst,
              rx      => rx,
              recData => recData,
              newData => newData);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        rx <= '1';
        wait for 10 * TbPeriod;
        --------------------------------------------------
        -- 			Open COM CH 						--
        --------------------------------------------------
        rx <= '0';
        wait for 1*bitTime;
        --------------------------------------------------
        -- Send LSB
        --------------------------------------------------
        rx <= '1';
        wait for 1*bitTime;
		--------------------------------------------------
		rx <= '0';
        wait for 7*bitTime;
		rx <= '1';
		-- Stop the clock and hence terminate the simulation
        wait for 2 * TbPeriod;
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_uartRecUbuntuTerminal of tb_uartRecUbuntuTerminal is
    for tb
    end for;
end cfg_tb_uartRecUbuntuTerminal;
